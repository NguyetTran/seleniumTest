package libraries;

import org.openqa.selenium.WebDriver;
import objects.BMIPage;

public class BMIFunctions extends BMIPage{

	public BMIFunctions(WebDriver driver) {
		super(driver);

	}
	
	public void selectMetricTab(){
		tabMetric.click();
	}
	
	public void fillBmiForm(String age, String gender, String height, String weight) throws InterruptedException{
		txtAge.clear();
		txtAge.sendKeys(age);
		
		if (gender.equalsIgnoreCase("male")){
			radMale.click();
		} else
			radFemale.click();
		
		Thread.sleep(2000);
		txtHeight.clear();
		txtHeight.sendKeys(height);
		
//		(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.id("txtweight")));
		Thread.sleep(2000);
		txtweight.clear();
		txtweight.sendKeys(weight);
		
//		(new WebDriverWait(driver, 60)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("btnCalculate")));
		Thread.sleep(2000);
		btnCalculate.click();
	}
	
	public String getBmiResult(){
		return lblBmiResult.getText();
	}

}
