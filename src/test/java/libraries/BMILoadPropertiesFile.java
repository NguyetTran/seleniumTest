package libraries;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BMILoadPropertiesFile {
	public static void loadPropertiesFile(String age, String gender, String height, String weight, String expectedResult, String filePath) throws FileNotFoundException, InterruptedException{
		Properties prop = new Properties();
		InputStream input = null;
		
		input = new FileInputStream(filePath);
		
		//Load properties file
		try {
			prop.load(input);
			
			//Get the values in properties file and grant the value
			age = prop.getProperty("age");
			gender = prop.getProperty("gender");
			height = prop.getProperty("height");
			weight = prop.getProperty("weight");
			expectedResult = prop.getProperty("expectedResult");
			
//			System.out.println("This is data test: " + age + " " + gender + " " + height + " " + weight + " " + expectedResult);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null){
				try {
					input.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

}
