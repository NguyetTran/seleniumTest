package day3;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import supports.CommonFunctions;

public class Main {

	public static void main(String[] args) throws MalformedURLException {
		WebDriver driver = null;
		
		// Open browser		
//		WebDriver driver = CommonFunctions.getBrowser("firefox");
		
//		CommonFunctions.visit(driver, "http://testingvn.com/");
//		CommonFunctions.fillIn(driver, "name", "q", "hello");
		
		//Launch RC
//		LaunchRC.RC("chrome", driver, "http://localhost:4444/wd/hub");
		
		/*
		//Exercise 15'
		driver = CommonFunctions.getBrowser("firefox");
		CommonFunctions.visit(driver, "http://www.calculator.net/bmi-calculator.html");
		
		CommonFunctions.getElement(driver, "xpath", ".//a[.='Metric Units']").click();
		CommonFunctions.fillIn(driver, "id", "cage", "30");
		CommonFunctions.fillIn(driver, "id", "cheightmeter", "165");
		CommonFunctions.fillIn(driver, "id", "ckg", "64");
		CommonFunctions.getElement(driver, "xpath", ".//input[@alt='Calculate']").click();
		
		String bmi_result = CommonFunctions.getElement(driver, "css", ".bigtext").getText();
		System.out.println("Result: " + bmi_result);
		//driver.quit(); */
		
		driver = CommonFunctions.getBrowser("firefox");
		CommonFunctions.visit(driver, "http://demoqa.com/registration/");
//		CommonFunctions.getElement(driver, "id", "menu-item-374").click();

		CommonFunctions.getElement(driver, "xpath", ".//*[@id='name_3_firstname']").sendKeys("Tuong");
		CommonFunctions.getElement(driver, "id", "name_3_lastname").sendKeys("Hong");
		CommonFunctions.getElement(driver, "class", "input_fields radio_fields").isSelected();
		CommonFunctions.getElement(driver, "class", "input_fields piereg_validate[required] radio_fields").click();
		
//		driver.quit();
	}
}
