package day3;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import supports.CommonFunctions;

public class LaunchRC {
	public static String RC(String browser, WebDriver driver, String host) throws MalformedURLException{
		if(browser.equalsIgnoreCase("firefox")) 
			driver = new RemoteWebDriver(new URL(host),DesiredCapabilities.firefox());

		else if(browser.equalsIgnoreCase("chrome"))
			driver = new RemoteWebDriver(new URL(host),DesiredCapabilities.chrome());
		
		else if(browser.equalsIgnoreCase("edge"))
			driver = new RemoteWebDriver(new URL(host),DesiredCapabilities.edge());
		
		else if(browser.equalsIgnoreCase("ie"))
			driver = new RemoteWebDriver(new URL(host),DesiredCapabilities.internetExplorer());
		
		else System.err.println("Cannot launch RC");
		
		CommonFunctions.visit(driver, "http://testingvn.com/");
		driver.quit();
		return browser;
	}
}
