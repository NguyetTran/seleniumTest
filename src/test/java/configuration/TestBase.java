package configuration;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import supports.CommonFunctions;

public class TestBase {
	public static WebDriver driver;
	public Properties prop = new Properties();
	public InputStream input = null;
	public String age, gender, height, weight, expectedResult;
	
	@Parameters({"browserName"})
	@BeforeClass
	public static void init(String browserName){
		driver = CommonFunctions.getBrowser("firefox");
	}
	
	@Parameters({"url"})
	@BeforeMethod
	public static void setUp(String url){
		CommonFunctions.visit(driver, url);
	}
	
	@Test
	@DataProvider(name = "userProperties")
	public Object[][] data() throws FileNotFoundException{
		input = new FileInputStream("./src/test/java/data/user.properties");
		
		//Load properties file
		try {
			prop.load(input);
			
			//Get the values in properties file and grant the value
			age = prop.getProperty("age");
			gender = prop.getProperty("gender");
			height = prop.getProperty("height");
			weight = prop.getProperty("weight");
			expectedResult = prop.getProperty("expectedResult");
			
			System.out.println("This is data test: " + age + " " + gender + " " + height + " " + weight + " " + expectedResult);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (input != null){
				try {
					input.close();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		
		return new Object[][]{
			new Object[]{age, gender, height, weight, expectedResult},
		};
	}

	
	@Test
	@DataProvider(name = "bmidata")
	public Object[][] testData() {
		return new Object[][] {
			new Object[] {"25","female","160","53","BMI = 20.70 kg/m2   (Normal)"},
			new Object[] {"25","male","180","73","BMI = 22.53 kg/m2   (Normal)"},
			new Object[] {"31","female","150","80","BMI = 35.56 kg/m2   (Obese Class II)"}
	    };
	  }

	@AfterMethod
	public void takeScreenShotIfFailure(ITestResult testResult) throws IOException {
		String screenShotFile;
		Date date = new Date();

		//Create format date
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

		//Create the file name with date time format then grant to "screenShotFile"
		screenShotFile = "E:\\\\test\\" + dateFormat.format(date).toString()+ " TestScreenShot" + ".jpg";

		if (testResult.getStatus() == ITestResult.FAILURE){
			System.out.println(testResult.getStatus());
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(screenShotFile));
		}
	}
	
	@AfterClass
	public static void tearDown(){
		driver.quit();
	}

}
