package supports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonFunctions {
	/**
	 * 
	 */
	
	public static WebElement getElement(WebDriver driver, String how, String locator){
		By el = null;
		if (how.equalsIgnoreCase("name")) el = By.name(locator);
		else if (how.equalsIgnoreCase("id")) el = By.id(locator);
		else if (how.equalsIgnoreCase("class")) el = By.className(locator);
		else if (how.equalsIgnoreCase("xpath")) el = By.xpath(locator);
		else if (how.equalsIgnoreCase("css")) el = By.cssSelector(locator);
		else if (how.equalsIgnoreCase("linktext")) el = By.linkText(locator);
		else System.err.println("Cannot locate element with "+how + locator);
		
		return driver.findElement(el);
	}
	
	public static WebDriver getBrowser(String browserName){
//		System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
//		WebDriver driver = new FirefoxDriver();
		
		WebDriver driver = null;
		
		if (browserName.equalsIgnoreCase("firefox")){
			System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		else if (browserName.equalsIgnoreCase("chrome")){
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		else if (browserName.equalsIgnoreCase("ie")){
			System.setProperty("webdriver.ie.driver", "./Drivers/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		
		else if (browserName.equalsIgnoreCase("edge")){
			System.setProperty("webdriver.edge.driver", "./Drivers/MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
		}		
		
		else System.err.println("Cannot open browser");
		
		return driver;
	}
	
	public static void visit(WebDriver driver, String url){
		driver.get(url);
		driver.manage().window().maximize();
	}
	
	public static void fillIn(WebDriver driver, String how, String locator, String inputStr){
		waitForElementPresent(driver, how, locator, 60);
		getElement(driver, how, locator).clear();
		getElement(driver, how, locator).sendKeys(inputStr);
	}
	
	public static void clickButton(WebDriver driver, String how, String locator){
		waitForElementPresent(driver, how, locator, 60);
		getElement(driver, how, locator).click();
	}
	
	public static void waitForElementPresent(WebDriver driver, String how, String locator, int timeout){
		By el = null;
		if (how.equalsIgnoreCase("name"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.name(locator)));
		else if(how.equalsIgnoreCase("id"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.id(locator)));
		else if(how.equalsIgnoreCase("class"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.className(locator)));
		else if(how.equalsIgnoreCase("xpath"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
		else if(how.equalsIgnoreCase("css"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
		else if(how.equalsIgnoreCase("linktext"))
			new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(By.linkText(locator)));
		else System.err.println("Cannot locate element with "+how + locator);
	}
	
//	public static void waitForeElemtDisable(WebDriver driver, String how, String locator, int timeout){
//		WebElement myDynamicElement;
//		myDynamicElement = (new WebDriverWait(driver, timeout)).until(ExpectedConditions.invisibilityOfElementLocated((By) getElement(driver, how, locator)));
//	}
	
	public static boolean verifyElementEnabled(WebDriver driver, String how, String locator){
		waitForElementPresent(driver, how, locator, 60);
		return getElement(driver, how, locator).isEnabled();
	}
	
	public static boolean verifyElementDisplayed(WebDriver driver, String how, String locator){
		waitForElementPresent(driver, how, locator, 60);
		return getElement(driver, how, locator).isDisplayed();
	}
	
	public static boolean verifyElementSelected(WebDriver driver, String how, String locator){
		waitForElementPresent(driver, how, locator, 60);
		return getElement(driver, how, locator).isSelected();
	}
	
	public static void Clicked(WebDriver driver, String how, String locator){
		waitForElementPresent(driver, how, locator, 60);
		getElement(driver, how, locator).click();
	}
	
	public static void Sendkey(WebDriver driver, String how, String locator, String inputStr){
		waitForElementPresent(driver, how, locator, 60);
		getElement(driver, how, locator).sendKeys(inputStr);
	}
}
