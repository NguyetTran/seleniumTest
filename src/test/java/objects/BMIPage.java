package objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BMIPage {
	public WebDriver driver;
	
	public BMIPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath=".//a[.='Metric Units']")
	public WebElement tabMetric;
	
	@FindBy(id="cage")
	public WebElement txtAge;
	
	@FindBy(id="csex1")
	public WebElement radMale;
	
	@FindBy(id="csex2")
	public WebElement radFemale;
	
    @FindBy(id="cheightmeter")
    public WebElement txtHeight;

    @FindBy(id="ckg")
    public WebElement txtweight;

    @FindBy(xpath=".//input[@alt='Calculate']")
    public WebElement btnCalculate;

    @FindBy(css=".bigtext")
    public WebElement lblBmiResult;
}
