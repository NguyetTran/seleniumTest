package modules;

import org.testng.annotations.Test;

/**
 * Created by dell on 3/18/2017.
 */
public class PriorityTest {
    @Test(priority=1 , enabled = false)
    public void registerAccount()
    {
        System.out.println("First register your account");
    }
    @Test(priority=2)
    public void sendEmail()
    {
        System.out.println("Send email after login");
    }
    @Test(priority = 3)
    public void login()
    {
        System.out.println("Login to the account after registration");
    }
}
