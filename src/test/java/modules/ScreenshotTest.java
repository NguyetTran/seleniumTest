package modules;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import supports.CommonFunctions;

import javax.print.attribute.standard.DateTimeAtCompleted;

/**
 * Created by dell on 3/21/2017.
 */
public class ScreenshotTest {
    public static WebDriver driver = null;

    @Parameters({"browserName"})
    @BeforeClass
    public static void init(String browserName){
        driver = CommonFunctions.getBrowser("firefox");
    }

    @Parameters({"url"})
    @BeforeMethod
    public static void setUp(String url){
        CommonFunctions.visit(driver, url);
    }

    @Test
    //Test google calculator
    public void googleCalculator(){
        try {
            //Set implicit wait of 10 seconds
//            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Thread.sleep(2000);
            //Write 2+2 in google textbox
            WebElement googleTextBox = driver.findElement(By.id("lst-ib"));
            googleTextBox.sendKeys("2+2");

            //Click on searchButton
//            WebElement searchButton = driver.findElement(By.name("btnK"));
//            searchButton.click();

            //Press Enter
            googleTextBox.sendKeys(Keys.ENTER);

            Thread.sleep(3000);

            //Get result from calculator
            WebElement calculatorTextBox = driver.findElement(By.id("cwos"));
            String result = calculatorTextBox.getText();

            //Intentionaly checking for wrong calculation of 2+2=5 in order to take screenshot for faling test
            Assert.assertEquals(result, "5");
        } catch (Exception e) {
            Assert.fail(); //To fail test in case of any element identification failure
        }
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        String screenShotFile;
        Date date = new Date();

        //Create format date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");

        //Create the file name with date time format then grant to "screenShotFile"
        screenShotFile = "E:\\\\test\\" + dateFormat.format(date).toString()+ " TestScreenShot" + ".jpg";

        if (testResult.getStatus() == ITestResult.FAILURE){
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(screenShotFile));
        }
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
