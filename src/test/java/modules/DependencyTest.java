package modules;

import org.testng.annotations.Test;

/**
 * Created by dell on 3/18/2017.
 * Dependency = chạy test case này trc test case kia
 */
public class DependencyTest {
    @Test(dependsOnMethods = { "testTwo" })
    public void testOne() {
        System.out.println("Test method one");
    }
    @Test
    public void testThree() {
        System.out.println("Test method three");
    }
    @Test
    public void testTwo() {
        System.out.println("Test method two");
    }
}
