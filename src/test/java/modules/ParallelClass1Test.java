package modules;

import org.testng.annotations.*;

/**
 * Created by dell on 3/18/2017.
 * Parallel will be run at the same time with the other class. They will be define in the "parallelclasstest.xml" file
 */
public class ParallelClass1Test {
    @BeforeClass
    public void beforeClass() {
        long id = Thread.currentThread().getId();
        System.out.println("Class 1: Before test-class. Thread id is: " + id);
    }

    @Test
    public void testMethodOne() {
        long id = Thread.currentThread().getId();
        System.out.println("Class 1: Sample test-method One. Thread id is: " + id);
    }

    @Test
    public void testMethodTwo() {
        long id = Thread.currentThread().getId();
        System.out.println("Class 1: Sample test-method Two. Thread id is: " + id);
    }

    @AfterClass
    public void afterClass() {
        long id = Thread.currentThread().getId();
        System.out.println("Class 1: After test-class. Thread id is: " + id);
    }
}
