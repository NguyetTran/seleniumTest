package modules;

import org.openqa.selenium.WebDriver;
import libraries.BMIFunctions;
import libraries.BMILoadExcelData;
import supports.CommonFunctions;

public class BMITest_Excel {

	public static void main(String[] args) throws Exception {
		WebDriver driver = null;
		String filePath = "./src/data/user.xlsx";
		String age, gender, height, weight, expectedResult;

		//Open the excel file
		BMILoadExcelData.setExcelFile(filePath, "Sheet1");

		//Load the values in Excel file and grant the values for each constants
		age = BMILoadExcelData.getCellData(1, 0);
		gender = BMILoadExcelData.getCellData(1, 1);
		height = BMILoadExcelData.getCellData(1, 2);
		weight = BMILoadExcelData.getCellData(1, 3);
		expectedResult = BMILoadExcelData.getCellData(1, 4);

//		System.out.println("age: "+ age);
//		System.out.println("gender: " + gender);
//		System.out.println("height: " + height);
//		System.out.println("weight: " + weight);
//		System.out.println("expectedResult" + expectedResult);

		driver = CommonFunctions.getBrowser("firefox");
		CommonFunctions.visit(driver, "http://www.calculator.net/bmi-calculator.html");
		BMIFunctions bmiPage = new BMIFunctions(driver);
		bmiPage.selectMetricTab();
		Thread.sleep(2000);
		bmiPage.fillBmiForm(age, gender, height, weight);
		Thread.sleep(2000);
//		AssertJUnit.assertEquals(expectedResult, bmiPage.getBmiResult());
		String result = bmiPage.getBmiResult();
		System.out.println(expectedResult + " -- compare with -- " + result);

		if(result.equals(expectedResult)){
			BMILoadExcelData.setCellData("Passed", 1, 5, filePath);
		}else {
			BMILoadExcelData.setCellData("Failed", 1, 5, filePath);
		}

		driver.quit();
	}

}
