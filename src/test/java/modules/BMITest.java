package modules;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import configuration.TestBase;
import libraries.BMIFunctions;

public class BMITest extends TestBase{
	
	@Test(dataProvider="userProperties")
	public static void calculate_bmi_test(String age, String gender, String height, String weight, String expectedResult) throws InterruptedException {
		System.out.println("Tuong - Starting open browser");
		BMIFunctions bmiPage = new BMIFunctions(driver);
		Thread.sleep(2000);
		bmiPage.selectMetricTab();
		Thread.sleep(2000);
		bmiPage.fillBmiForm(age, gender, height, weight);
		Thread.sleep(2000);
		AssertJUnit.assertEquals(expectedResult, bmiPage.getBmiResult());
	}
	
	@Test
	public void TC1(){
		System.out.println("This is TC1");
	}
	
	@Test
	public void TC2(){
		System.out.println("This is TC2");
	}	
}
